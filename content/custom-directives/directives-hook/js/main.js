Vue.directive('fixed',{
    bind(el,binding){
        el.style.position = 'fixed';
        if(binding.expression){
            el.style.top = binding.expression + 'px';
        }
        if(binding.arg){
            let color = 'red';
            if(binding.modifiers){
                color = Object.keys(binding.modifiers)[0];
            }
            el.style.backgroundColor = color;
        }
    }
});

Vue.directive('destacar',{
    bind(el,binding){
        console.log(binding);
        
        Object.keys(binding.value).forEach(e=>{
            el.style[e] = binding.value[e]
        }) 
    }
})

new Vue({
    el:'#app'
})