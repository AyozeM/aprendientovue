import Vue from 'vue';
Vue.mixin({
  created(){
    this.saludar(this.$options.message);
    
  },
  methods:{
    saludar(message){
      alert(`the message is: ${message}`)
    }
  }
})

export const mixin = {
    created(){
      console.log('desde mixin');
      
    },
    name: 'app',
    data () {
      return {
        toAdd:null,
        tecnologias:[
          'ionic',
          'angular',
          'react',
          'node'
        ]
      }
    },
    methods:{
      add(){
        this.tecnologias.push(this.toAdd);
        this.mongotoAdd = null;
      },
    },
    message:'componente'
}