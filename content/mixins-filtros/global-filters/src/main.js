import Vue from 'vue'
import App from './App.vue'

Vue.filter('uppercase',valor=>valor.toUpperCase());

new Vue({
  el: '#app',
  render: h => h(App)
})
