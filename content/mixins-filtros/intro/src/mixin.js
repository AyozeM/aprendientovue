export const mixin = {
    name: 'app',
    data () {
      return {
        toAdd:null,
        tecnologias:[
          'ionic',
          'angular',
          'react',
          'node'
        ]
      }
    },
    methods:{
      add(){
        this.tecnologias.push(this.toAdd);
        this.mongotoAdd = null;
      }
    }
}