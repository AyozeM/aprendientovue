import {getters} from './getters';
import {mutations} from './mutations';

const carro  = [];

export default {
    state: carro,
    mutations,
    getters
}