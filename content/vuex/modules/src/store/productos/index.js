import {mutations} from './mutations';

const productos = [
    {name:'papas',price:12},
    {name:'uvas',price:6},
    {name:'castañas',price:3}
];

export default {
    state:productos,
    mutations
}