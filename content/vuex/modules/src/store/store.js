import Vue from 'vue';
import Vuex from 'vuex';
import productos from './productos/index';
import carro from './carro/index';

Vue.use(Vuex);

export const store = new Vuex.Store({
    modules:{
        productos,
        carro
    }
});
