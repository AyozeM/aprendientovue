import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state:{
        cantidad:0
    },
    mutations:{
        aumentar: (state,cantidad) => state.cantidad += cantidad,
        disminuir: (state,cantidad) => state.cantidad -= cantidad
    },
    actions:{
        aumentarAsync: (context, cantidad) =>{
            setTimeout(()=>context.commit('aumentar',cantidad),2000)
        },
        disminuirAsync: ({commit},{valor}) =>{
            setTimeout(()=>commit('disminuir',valor),2000)
        }
    }
})