import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state:{
        tareas:[
            {title:'limpiar la azotea', completed:true},
            {title:'hacer la compra del mes', completed:false},
            {title:'formatear el ordenador', completed:false},
            {title:'aprender vue.js', completed:false},
            {title:'cambio de aceite', completed:true},
            {title:'comprar cable usb', completed:false}
        ]
    },
    getters:{
        tareasCompletadas: state=> state.tareas.filter(e=>e.completed).length
    }
}); 