import Vue from 'vue'
import App from './App.vue'
import VueRouer from 'vue-router';
import {routes} from './routes/routes';
import {store} from './store/store';

Vue.use(VueRouer);

const router = new VueRouer({routes});

//Global guard
router.beforeEach((to,from,next)=>{
  console.log('Acceso a ruta');
  
  next(store.state.auth);
});

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
