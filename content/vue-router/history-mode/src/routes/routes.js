import home from '../components/home.vue';
import contacto from '../components/contacto.vue';
import pageNotFound from '../components/pageNotFound.vue';

export const routes = [
    { path :'/', component: home },
    { path :'/contacto', component: contacto },
    // La ruta de 404 siempre han de ir en el ultimo lugar
    { path :'*', component: pageNotFound }
]