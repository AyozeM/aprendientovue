import Vue from 'vue';
import VueRouter from 'vue-router';

import home from '../components/home.vue';
import bio from '../components/bio.vue';

Vue.use(VueRouter);

const routes = [
    { path:'/', component: home },
    { path:'/bio', component: bio },
];

const router = new VueRouter({
    routes,
    scrollBehavior(to,from,savedPosition){
        const position = {};
        if(to.hash){
            position.selector = to.hash;
        }else{
            position.x = 200;
            position.y = 500
        }
        return position;
    }
});

export default router;