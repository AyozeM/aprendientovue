import home from '../components/home.vue';
import equipo from '../components/equipo.vue';
import equipoJuan from '../components/equipoJuan.vue';
export const routes = [
    {path:'/', component:home},
    {path:'/equipo', component:equipo, children:[
        {path:'juan',component:equipoJuan}
    ]},
];