import home from '../components/home.vue';
import equipo from '../components/equipo.vue';
import usuario from '../components/usuario.vue';
import usuarioBio from '../components/usuarioBio.vue';
import usuarioFoto from '../components/usuarioFotos.vue';

export const routes = [
    { path:'/', component:home },
    { path:'/equipo/:id', component:equipo, children:[
        { path:'', component:usuario, children:[
            { path:'fotos', component:usuarioFoto},
            { path:'bio', component:usuarioBio},
        ]},
    ] },
]