import usuario from '../components/usuario.vue';
import home from '../components/home.vue';

export const routes = [
    {path:'/',component:home},
    {path:'/usuario/:id',component:usuario}
]