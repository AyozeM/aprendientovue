import Vue from 'vue';
import VueRouter from 'vue-router';

import home from '../components/home.vue';
import contacto from '../components/contacto.vue';

Vue.use(VueRouter);

const routes = [
    { path:'/', component: home },
    { path:'/contacto', component: contacto },
];

const router = new VueRouter({routes});

export default router;