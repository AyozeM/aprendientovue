import Vue from 'vue';
import VueRouter from 'vue-router';

import home from '../components/home.vue';
import contacto from '../components/contacto.vue';
import usuario from '../components/usuario.vue';

Vue.use(VueRouter);


const routes = [
    { path:'/', component: home },
    { path:'/contacto', component: contacto },
    { path:'/usuario/:name', component: usuario },
]

export const router = new VueRouter({
    routes
});