import home from '../components/home.vue';
import equipo from '../components/equipo.vue';
import usuario from '../components/usuario/usuario.vue';
import usuarioBio from '../components/usuario/usuarioBio.vue';
import usuarioFoto from '../components/usuario/usuarioFoto.vue';

/**
 * Redirecciona a un segmento concreto
 */
const redireccionSegmento = {path:'/test', component:home, redirect:'/'}
/**
 * Redirecciona a un nombre
 */
const redireccionNombre = {path:'/test', component:home, redirect:{name:'home'}}

const alias = {path:'/testAlias', component:home, alias:'/otraprueba'}


export const routes = [
    {path:'/', component:home, name:'home'},
    {path:'/testAlias', component:home, alias:'/otraprueba'},
    { path:'/equipo/:id', component:equipo, children:[
        { path:'', name:'equipo', components:{
            default: usuario,
            bio: usuarioBio,
            fotos: usuarioFoto
        } }
    ] }
];