import home from '../components/home.vue';
import users from '../components/users/users.vue';
import contacto from '../components/contacto.vue';
import pageNotFound from '../components/pageNotFound.vue'

export const routes  = [
    {path:'/', component:home},
    {path:'/users', component:users},
    {path:'/contacto', component:contacto},
    {path:'*', component:pageNotFound},
];