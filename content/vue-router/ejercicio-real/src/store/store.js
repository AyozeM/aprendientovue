import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state:{
        allUsers:[]
    },
    mutations:{
        setAllUsers: (state,users)=>{
            state.allUsers = users;
        }
    },
    actions:{
        getUsers: (context,numberOfUsers)=>{
            axios.get(`https://randomuser.me/api/?results=${numberOfUsers}`).then(
                users =>{
                    console.log(users);
                    context.commit('setAllUsers',users.data.results);
                }
            );
        }
    }
});