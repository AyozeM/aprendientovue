import home from '../components/home.vue'
import equipo from '../components/equipo.vue'
import usuario from '../components/usuario/usuario.vue';
import usuarioBio from '../components/usuario/usuarioBio.vue'
import usuarioFoto from '../components/usuario/usuarioFoto.vue'

export const routes = [
    {path:'/',component:home},
    {path:'/equipo/:id', component:equipo, children:[
        {path:'', name: 'equipo',component:usuario, children:[
            {path:'bio', component:usuarioBio, name:'bio'},
            {path:'fotos', component:usuarioFoto, name:'fotos'},
        ]}
    ]}  
]