import Vue from 'vue';
import VueRouter from 'vue-router';
import home from '../components/home.vue';
import usuario from '../components/usuario.vue';
import posts from '../components/posts.vue';

Vue.use(VueRouter);

const routes = [
    { path:'/', component: home },
    { path:'/usuario/:name', component: usuario },
    { path:'/posts/:name', component: posts },
];

const router = new VueRouter({routes});

export default router;