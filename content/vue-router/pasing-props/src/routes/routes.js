import home from '../components/home.vue';
import contacto from '../components/newsletter.vue';

export const routes = [
    {path:'/', component:home},
    /* {path:'/contacto', component:contacto} */
    {path:'/contacto', component:contacto, props:{newsletter:true}}
];