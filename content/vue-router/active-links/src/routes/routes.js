import home from '../components/home.vue';
import contacto from '../components/contacto.vue';
import user from '../components/user.vue';

export const routes= [
    { path:'/', component:home },
    { path:'/contacto', component:contacto },
    { path:'/user:data', component:user }
]