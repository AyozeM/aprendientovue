import home from '../components/home.vue';
import contacto from '../components/contacto.vue';
import {store} from '../store/store';

export const routes = [
    { path:'/', component: home },
    { path:'/contacto', component: contacto, beforeEnter:((to,from,next)=>{
        next(store.state.auth);
    }) },
]