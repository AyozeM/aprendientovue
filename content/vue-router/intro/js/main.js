const home = Vue.component('home',{
    template:'<h1>Home</h1>'
})
const contacto = Vue.component('contacto',{
    template:'<h1>contacto</h1>'
})

const routes = [
    {path: '/', component:home},
    {path: '/contacto', component:contacto}
];

const router = new VueRouter({
    routes
})
const vm = new Vue({
    router, 
    el:'#app', 
    data:{ 
        showConsole:false 
    }, 
    methods:{ 
        toggleConsole(){ this.showConsole = !this.showConsole; 
        } 
    } 
});