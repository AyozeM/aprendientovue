import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '../store/store';

import confidencial from '../components/confidencial.vue';
import unauthorized from '../components/unauthorized.vue';
import home from '../components/home.vue';


Vue.use(VueRouter);


const routes = [
    { path:'/', component: home },
    { path:'/confidencial', component: confidencial, meta: { privado: true } },
    { path:'/unauthorized', component: unauthorized, name:'unauthorized' }
];

const router = new VueRouter({routes});

router.beforeEach((to,from,next)=>{
    if(to.meta.privado && !store.state.auth){
        console.error('Error 405');
        next({name:'unauthorized'});
    }else{
        next();
    }
});

export default router;