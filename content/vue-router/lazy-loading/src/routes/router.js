import Vue from 'vue';
import VueRouter from 'vue-router';

import home from '../components/home.vue';
//import lazy from '../components/lazy.vue';
const lazy = resolve => {
    require.ensure(['../components/lazy.vue'],()=>{
        resolve(require('../components/lazy.vue'));
    })
}

Vue.use(VueRouter);

const routes = [
    { path:'/', component: home },
    { path:'/lazy', component: lazy }
];

const router = new VueRouter({routes});

export default router;