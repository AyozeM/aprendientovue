const vm = new Vue({
    el:'#app',
    data:{
        form:{
            exp:null,
            years:0,
            origen:'',
            anecdotas:''                    
        }
    },
    computed:{
        descriptionError(){
            let val = this.form.anecdotas.trim();
            let message = null;
            switch (true) {
                case val.length == 0:
                    message = 'El campo es requerido';
                    break;
                case val.length < 5:
                    message = 'La descripcion es demasiado corta';
                    break;
                case val.length > 20:
                    message = 'La descripcion es demasiado larga'
                    break;
            }
            return message;
        }
    }
})