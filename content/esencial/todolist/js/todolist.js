const vm = new Vue({
    el:'#app',
    data:{
        new_task:null,
        tasks:[
            {
                title:'hacer la compra',
                pending:true
            },
            {
                title:'limpiar el polvo',
                pending:false
            },
            {
                title:'Sacar a pasear al perro',
                pending:true
            }
        ]
    },
    computed:{
        checkPendingTask(){
            return this.tasks.filter(e=>e.pending);
        },
        completedTasks(){
            return this.tasks.filter(e=>!e.pending).length;
        },
        percentajeCompleted(){
            return (this.completedTasks*100 ) / 3;
        }
    },
    methods:{
        createTask(pending){
            this.tasks.push({title:this.new_task,pending:pending});
            this.new_task = null;
        },
        completeTask:function(index){
            this.tasks[index].pending = !this.tasks[index].pending;
        }
    }
});