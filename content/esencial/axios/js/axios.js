const urlapi = 'https://randomuser.me/api/';
const vm = new Vue({ 
    el:'#app', 
    data:{ 
        showConsole:false ,
        users : []
    }, 
    methods:{
        toggleConsole(){ 
            this.showConsole = !this.showConsole; 
        } 
    },
    mounted(){
        console.log('instancia montada');
        axios.get(`${urlapi}?results=500`)
            .then(
                response => {
                    this.users = response.data.results;
                    console.log(this.users);
                    
                }
            );
    } 
});