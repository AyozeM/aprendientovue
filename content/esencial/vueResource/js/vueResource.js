const vm = new Vue({ 
    el: '#app', 
    data: {
        showConsole:true,
        users:[]
    },
    mounted(){
        this.cargarPersonas();
    },
    methods:{
        toggleConsole(){
            this.showConsole = !this.showConsole;
        },
        cargarPersonas(){
            this.$http.get('https://randomuser.me/api/?results=500')
                .then(
                    data=>{
                        this.users = data.body.results;
                    },
                    error=>console.error('ha ocurrido unerror')
                );
        }
    }
});