Vue.component('props-validadas',{
    template:'#props-validadas-template',
    props:{
        nombre: String,
        edad:{
            type: Number,
            validator: value => value > 0
        },
        dni:{
            type: String,
            required:true
        },
        vueisfun:{
            type: Boolean,
            default: true
        },
        objeto:{
            type: Object,
            default(){
                return {
                     carcasa:false,
                     valor:1
                }
            },
            validator: val => val.valor > 0
        }
    }
});

const vm = new Vue({
    el:'#app',
    data:{
        showConsole:false,
        objeto:{
            carcasa:true,
            valor:10
        }
    },
    methods:{
        toggleConsole(){
            this.showConsole = !this.showConsole;
        }
    }
})