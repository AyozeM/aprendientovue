Vue.component('prompt-user',{
    template:'#prompt-template',
    data(){
        return{
            name:null
        }
    },
    methods:{
        sendName(){

        },
        close(){
            this.$emit('ocultar');
        }
    }
});

const vm = new Vue({ 
    el:'#app', 
    data:{ 
        showConsole:false ,
        togglePrompt:true
    }, methods:{ 
        toggleConsole(){ 
            this.showConsole = !this.showConsole; 
        } 
    } 
});