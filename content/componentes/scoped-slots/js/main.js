Vue.component('todo-list',{
    template:'#todoList-template',
    props:['listado']
});

const vm = new Vue({ 
    el:'#app', 
    data:{ 
       showConsole:false,
       list:[
           {
               titulo:'hacer la compra'
           },
           {
               titulo:'hacer la compra'
           },
           {
               titulo:'fregar la azotea'
           }
       ]
    }, 
    methods:{ 
        toggleConsole(){ 
            this.showConsole = !this.showConsole; 
        } 
    } 
});