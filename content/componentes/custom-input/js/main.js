Vue.component('password-input',{
    template:'#password-input-template',
    data(){
        return{
            error:null,
            invalidas:['root', 'admin', 'user']
        }
    },
    props:['clave'],
    methods:{
        checkPassword(password){
            if(this.invalidas.includes(password)){
                this.error = 'La contraseña ha de ser más segura';
                this.$refs.pass.value = password = '';
            }else{
                this.error = null;
            }
            this.$emit('input',password);
        }
    }
});
const vm = new Vue({ 
    el: '#app', 
    data: { 
        showConsole: false,
        clave:'defecto' 
    }, 
    methods: { 
        toggleConsole() { this.showConsole = !this.showConsole; 
        } 
    } 
});