Vue.component('panel-1',{
    template:'#panel-1-template'
});
Vue.component('panel-2',{
    template:'#panel-2-template'
});
Vue.component('panel-3',{
    template:'#panel-3-template'
});

const vm = new Vue({ 
    el:'#app', 
    data:{ 
        showConsole:false,
        selected:'panel-1'
    }, 
    methods:{ 
        toggleConsole(){ 
            this.showConsole = !this.showConsole; 
        } 
    } 
});