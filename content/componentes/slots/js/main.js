Vue.component('message-alert',{
    template:'#message-template',
    props:{
        tipo:{
            default:'info'      
        },
        posicion:{
            default:'right-top'
        }
    },
    computed:{
        type(){
            let type;
            switch (this.tipo) {
                case 'ok':
                    type = 'alert-success'
                    break;
                case 'info':
                    type = 'alert-primary'
                    break;
                case 'warning':
                    type = 'alert-warning'
                    break;
                case 'error':
                    type = 'alert-danger'
                    break;
                default:
                    console.error(`El tipo ${this.tipo} no está disponible`)
                    break;
            }
            return type;
        },
        position(){
            let pos;
            switch (this.posicion) {
                case 'left-top':
                    pos = 'left-top'
                    break;
                case 'right-top':
                    pos = 'right-top'
                    break;
                case 'left-bottom':
                    pos = 'left-bottom'
                    break;
                case 'right-bottom':
                    pos = 'right-bottom'
                    break;
                default:
                    console.error(`La posicion ${this.position} no está disponible`);
                    break;
            }
            return pos;
        }
    },
    mounted(){
        this.isClose = false;
    },
    data(){
        return {
            isClose:null
        }
    },
    methods:{
        close(){
            this.isClose = true;
        }
    }
});

const vm = new Vue({ 
    el:'#app', 
    data:{ 
        showConsole:false,
        tipo:'ok',
        posicion:'left-top'
    }, 
    methods:{ 
        toggleConsole(){ 
            this.showConsole = !this.showConsole; 
        } 
    } 
});