Vue.component('tasks-list-dependiente',{
    props:['tasks'],
    template:`
        <div>
            <p v-for="task in tasks">
                <input type="checkbox" :checked="task.pending">
                <span :class="{'tachado':!task.pending}" >{{ task.title }}</span>
            </p>
        </div>
    `
});

Vue.component('task-list-independiente',{
    template:`
        <div>
            <p v-for="(task,i) in tareas">
                <input type="checkbox" :checked="task.pending" :id="i" v-model="task.pending">
                <label :for="i" :class="{'tachado':!task.pending}" >{{ task.title }}</label>
            </p>
        </div>
    `,
    data(){
        return{
            tareas:[
                {
                    title:'Hacer la compra',
                    pending:true
                },
                {
                    title:'Fregar la azotea',
                    pending:false
                },
                {
                    title:'Enviar correo',
                    pending:true
                }
            ] 
        }
    }
});

const vm = new Vue({ 
    el:'#app', 
    data:{ 
        showConsole:false,
        tareasLocales:[
            {
                title:'Hacer la compra',
                pending:true
            },
            {
                title:'Fregar la azotea',
                pending:false
            },
            {
                title:'Enviar correo',
                pending:true
            }
        ] 
    }, 
    methods:{ 
        toggleConsole(){ 
            this.showConsole = !this.showConsole; 
        } 
    } 
});