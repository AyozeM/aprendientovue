const bus = new Vue();

Vue.component('usuarios', {
    template:'#usuarios-template',
    mounted(){
        axios.get('https://randomuser.me/api/?results=500')
        .then(datos=>{
            this.users = datos.data.results.map(usuario=>{
                return{
                    nombre:`${usuario.name.first} ${usuario.name.last}`,
                    title:usuario.name.title,
                    email: usuario.email,
                    foto: usuario.picture.medium
                }
            });
        });
        
    },
    data(){
        return{
            users:[],
            filterExpr:''
        }
    },
    computed:{
        filterUsers(){
            return this.users.filter(e=>{
                        const reg = new RegExp(this.filterExpr);
                        return reg.test(e.nombre);
                    });
        }
    },
    created(){
        bus.$on('find',searchText=>{
            this.filterExpr = new RegExp(`^${searchText}\s*`);
        })
    }
});
Vue.component('usuario',{
    template:'#usuario-template',
    props:['user'],
    computed:{
        hrefEmail(){
            return `mailto:${this.user.email}`
        }
    }
});
Vue.component('buscador',{
    template:'#buscador-template',
    data(){
        return{
            searchText:null
        }
    },
    methods:{
        find(){
            let searchText = this.searchText.trim();
            bus.$emit('find',searchText);            
        }
    }
})
const vm = new Vue({ 
    el:'#app', 
    data:{ 
        showConsole:false 
    }, 
    methods:{ 
        toggleConsole(){ this.showConsole = !this.showConsole; 
        } 
    } 
});