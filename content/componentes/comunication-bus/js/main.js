const bus = new Vue();

Vue.component('producto',{
    template:'#producto-template',
    props:['product'],
    data(){
        return{
            name: this.product.name,
            cantidad:0
        }
    },
    computed:{
        price(){
            return parseFloat(this.product.price.toFixed(2));
        }
    },
    methods:{
        calcular(){
            return this.cantidad*this.price;
        },
        addProduct(){
            this.cantidad++;
            bus.$emit('add',this.price)
        },
        removeProduct(){
            if(this.cantidad > 0){
                this.cantidad--;
                bus.$emit('remove',this.price)
            }
        }
    }
});
Vue.component('listado-productos',{
    template:'#listado-productos-template',
    props:['productos']
});
Vue.component('precio-total',{
    template:'#precio-total-template',
    data(){
        return{
            produtosTotal:0,
            precioTotal:0
        }
    },
    computed:{
        precio(){
            return this.precioTotal.toFixed(2);
        }        
    },
    created(){
        bus.$on('add',(precio)=>{
            this.produtosTotal++;
            this.precioTotal += precio;
        });
        bus.$on('remove',(precio)=>{            
            this.produtosTotal--;
            this.precioTotal -= precio;
        });
    }
});

const vm = new Vue({ 
    el:'#app', 
    data:{ 
        showConsole:false,
        productos:[
            {
                name:'pan',
                price:0.5
            },
            {
                name:'jamon serrano',
                price:7.5
            },
            {
                name:'queso blanco',
                price:3.75
            }
        ] 
    }, 
    methods:{ 
        toggleConsole(){ this.showConsole = !this.showConsole; 
        } 
    } 
});