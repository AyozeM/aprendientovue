const db = firebase.database();
const chat1 = '/chats/chat1'
const gatos = [
    { nombre:'gato', edad:5, color:'blanco y negro' },
    { nombre:'garfield', edad:2, color:'atigrado' },
    { nombre:'copito de nieve', edad:10, color:'blanco' },
    { nombre:'pantera', color:'negro' },
]
const ejercicioUsusario = {
    reference:db.ref('/users'),
    data:{
        perfil:{
            username:null,
            password:null
        },
    },
    methods:{
        saveUser(){
            db.ref('/users').set(this.perfil);
        },
    }
}
const ejercicioChat = {
    reference: db.ref('/chats/chat1'),
    data:{
        chat:{
            user: 'ayoze',
            message:null
        },
        messages:[]
    },
    methods:{
        editarMensaje(mensaje,key){
            console.log(mensaje.target.innerText,key);
            //modo update (inseguro)
            /*db.ref(`${chat1}/${key}`).update({
                message: mensaje.target.innerText
            }) */
    
            //modo transaction (seguro)
            
            ejercicioChat.reference.child(key).child('message').transaction(
                message => mensaje.target.innerText,
                (error, commited, snapshot) =>{
                    if(error){
                        console.error('Error fatal', error);
                    }else if(!commited){
                        console.warn('Transaccion fallida');
                        
                    }else{
                        console.log('Transaccion realizada');
                    }
                }
            );
        },
        writeMessage(message){
            this.messages = message;
        },
        sendMessage(){
            // se esta guardando el objeto que devuelve, del cual puede obtenerse la clave o bien una promesa a devolver
            const newChat = ejercicioChat.reference.push(this.chat);
            newChat.then(
                () => {
                    console.log('Todo correcto');
                    this.chat.message = null;
                },
                error => {
                    console.error('Algo ha fallado', error);
                    alert('fallo! mira la consola')
                }
            );
            console.log(newChat.key);
        },
        removeMessage(key){
            db.ref(`${chat1}/${key}`).remove()
        }   
    }
}
const ejercicioOrder = {
    reference: db.ref('/gatos'),
    data:{
        cats:[],
        filters:{
            n:null
        }
    },
    methods:{
        orderBySecondKey(key){
            ejercicioOrder.reference.orderByChild(key).on('child_added', snapshot => {
                console.log(snapshot.val())
            })
        },
        orderByKey(){
            ejercicioOrder.reference.orderByKey().on('child_added', snapshot => console.log(snapshot.val()))
        },
        orderByValue(){
            ejercicioOrder.reference.orderByValue().on('child_added', snapshot => console.log(snapshot.val()))
        },
        limitsFirsts(){
            ejercicioOrder.reference.orderByChild('edad').limitToFirst(this.filters.n).on('child_added', snapshot => console.log(snapshot.val()))
        },
        limitsLasts(){
            ejercicioOrder.reference.orderByChild('edad').limitToLast(this.filters.n).on('child_added', snapshot => console.log(snapshot.val()))
        }
    },
}

const vm = new Vue({
    el:'#app',
    created(){
        ejercicioChat.reference.on('value',snapshot => this.writeMessage(snapshot.val()));
        gatos.forEach(e=>{
            ejercicioOrder.reference.child(e.nombre).set({
                edad:e.edad || null,
                color: e.color
            })
        });
    },
    data:{
        ...ejercicioChat.data,
        ...ejercicioOrder.data,
        ...ejercicioUsusario.data
    },
    methods:{
        ...ejercicioChat.methods,
        ...ejercicioOrder.methods,
        ...ejercicioUsusario.methods
    }
})