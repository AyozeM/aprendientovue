
const vm = new Vue({
    el: '#app',
    data: {
        ejemplos: [
            'esencial/formulario',
            'esencial/todolist',
            'esencial/transitions',
            'esencial/vueResource',
            'esencial/axios',
            'componentes/componentes1',
            'componentes/propiedades',
            'componentes/slots',
            'componentes/scoped-slots',
            'componentes/custom-events',
            'componentes/comunication-bus',
            'componentes/dinamyc-components',
            'componentes/custom-input',
            'componentes/ejercicio',
            'vue-router/intro'
        ],

    },
    computed: {
        paths: function () {
            const paths = [];
            this.ejemplos.forEach(e => {
                paths.push({
                    url: `content/${e}`,
                    text: `ejemplo ${e}`
                });
            });
            return paths;
        }
    }
});