#!/bin/bash
root="./content/$1"

contenthtml="<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>Document</title>
    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css\">
    <link rel=\"stylesheet\" href=\"/css/default.css\">
    <link rel=\"stylesheet\" href=\"css/style.css\">
</head>
<body>
    <div id=\"app\" class=\"container\">
        <div class=\"row justify-content-between\">
            <h1 class=\"flex-fill\">$1 en vue</h1>
            <button class=\"btn btn-primary col-2\" @click=\"toggleConsole\">Show console</button>
        </div>
            <hr>
        <pre class=\"consola\" v-show=\"showConsole\">{{ \$data }}</pre>
    </div>
    <script src=\"https://cdn.jsdelivr.net/npm/vue/dist/vue.js\"></script>
    <script src=\"js/main.js\"></script>
</body>
</html>"
contentJs="const vm = new Vue({
    el:'#app',
    data:{
        showConsole:false
    },
    methods:{
        toggleConsole(){
            this.showConsole = !this.showConsole;
        }
    }
})"
#creando directorios
mkdir $root
mkdir "$root/css"
mkdir "$root/js" 
#creando ficheros
echo $contenthtml > "$root/index.html"
touch "$root/css/style.css"
echo $contentJs > "$root/js/main.js"
echo "ejemplo creado";